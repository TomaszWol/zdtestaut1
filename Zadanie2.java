package zestaw1;

import java.util.Scanner;

public class Zadanie2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        System.out.println("Min: " + Math.min(a, Math.min(b, c)));
        System.out.println("Max: " + Math.max(a, Math.max(b, c)));
    }
}
