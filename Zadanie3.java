package zestaw1;

import java.util.Scanner;

public class Zadanie3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj wagę [kg]: ");
        double weight = scanner.nextDouble();

        System.out.print("Podaj wzrost [cm]: ");
        double height = scanner.nextDouble();

        double bmi = weight / ((height/100.0) * (height/100.0));
        System.out.println("BMI: " + bmi);

        if(bmi < 18.5) {
            System.out.println("niedowaga");
        } else if(bmi > 24.9) {
            System.out.println("nadwaga");
        } else {
            System.out.println("waga prawidłowa");
        }

    }
}
