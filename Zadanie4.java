package zestaw1;

import java.util.Scanner;

public class Zadanie4 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj dochód: ");
        double income = scanner.nextDouble();

        double tax;
        if(income > 85_528.0) {
            tax = 14_839.02 + 0.32 * (income - 85_528.0);
        } else {
            tax = 0.18 * income;
        }
        System.out.println("Podatek: " + tax);
    }
}
