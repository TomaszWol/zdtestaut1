package zestaw1;

import java.util.Scanner;

public class Zadanie1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double celsius = scanner.nextDouble();
        double fahrenheit = 1.8 * celsius + 32.0;

        System.out.println(fahrenheit);
    }
}
